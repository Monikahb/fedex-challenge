import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { routes } from './app.routes';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@shared/shared.module';
import { MdlModule } from '@angular-mdl/core';

export const APP_IMPORTS = [
  RouterModule.forRoot(routes),
  HttpClientModule,
  ReactiveFormsModule,
  SharedModule,
  MdlModule
];
