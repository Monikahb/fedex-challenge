import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SignupService } from '@core/services';
import { emailValidator } from '../../../shared/validators/email.validator';
import { personalPasswordError } from '@app/shared/validators/password.validator';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  signUpForm: FormGroup;
  submitAttempt = false; 
  isSubmitting = false;
  signUpComplete = false;

  signUpFailed = false;
  signUpError = '';
  success = "Success"
  /**
   * 
   */
  constructor(
    private formBuilder: FormBuilder,
    private signupService: SignupService) { }

  get username(){
    return this.signUpForm.get('username')
  }
  /**
   * 
   */
  ngOnInit() {
    this.signUpForm = this._createForm();
  }

  /**
   * 
   */
  signUp($event) {
    $event.stopPropagation();

    this.submitAttempt = true;
    this.signUpFailed = false;
    this.signUpComplete = false;
    console.log(this.signUpForm.value)
    if (this.signUpForm.valid) {

      const signUpData = this.signUpForm.value;
      console.log(signUpData)
      this.isSubmitting = true;
      this.signupService.signUp(signUpData).subscribe(result => {
        this.isSubmitting = false;
        this.signUpComplete = true;
        this.signUpForm.reset()
      }, error => {
        this.isSubmitting = false;
        this.signUpFailed = true;
        this.signUpError = 'An unknown error occured. Please try again!';
      });
    }
  }

  /**
   * 
   */
  formControlIsInvalid(identifier): boolean {
    if ( ! this.submitAttempt ) {
      return false;
    }
    console.log(this.signUpForm.get('username').hasError('validateEmail'));
    return this.submitAttempt && this.signUpForm.get(identifier).invalid;
  }
  /**
   * 
   */
  private _createForm(): FormGroup {
    return this.formBuilder.group({
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      username: ['', [Validators.required, emailValidator('username')]],
      password: ['', [Validators.required, Validators.minLength(8)]]
    },{ validator: personalPasswordError });
  }
}
