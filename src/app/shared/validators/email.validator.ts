
import { AbstractControl, ValidatorFn } from '@angular/forms';

export function emailValidator(controlName: string): ValidatorFn {

  return (control: AbstractControl) => {
    const valid = RegExp('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$').test(control.value);
    return valid ? null : {validateEmail: true};
  }
}
