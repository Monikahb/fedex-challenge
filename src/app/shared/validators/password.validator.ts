import { AbstractControl, ValidatorFn, FormControl, FormGroup } from '@angular/forms';

export const personalPasswordError: ValidatorFn = (ctrl: FormGroup) => {
  const password = ctrl.get('password') as FormControl;
  const firstname = ctrl.get('firstname') as FormControl;
  const lastname = ctrl.get('lastname') as FormControl;
  const valid = !((password.value || '').includes(firstname.value) && (password.value || '').includes(lastname.value));

  return valid ? null : { personalPasswordError: true}
}