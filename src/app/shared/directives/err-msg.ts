import { Directive, HostBinding } from '@angular/core';

@Directive({
  selector: '[dirErrorMsg]'
})
export class ErrorMsgDirective {
  @HostBinding('class.formfield-error-msg') errorMsgClass = true;
  constructor() { }
}
