import {Directive} from '@angular/core';

/** Prefix to be placed at the front of the form field. */
@Directive({
  selector: '[dirPrefix]',
})
export class PrefixDirective {}
