import {Directive} from '@angular/core';

/** Postfix to be placed at the front of the form field. */
@Directive({
  selector: '[dirPostfix]',
})
export class PostfixDirective {}
