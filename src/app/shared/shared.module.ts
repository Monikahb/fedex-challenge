import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SHARED_DECLARATIONS } from './shared.declarations';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    SHARED_DECLARATIONS
  ],
  exports: [
    SHARED_DECLARATIONS,
    CommonModule
  ]
})
export class SharedModule { }
