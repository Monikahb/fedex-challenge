import { Component, Input } from '@angular/core';
export type AlertLevel = 'info'|'error'|'success'|'warning'|'neutral' |'critical';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent {

  @Input() type: AlertLevel = 'info';
  @Input() text: string;
  @Input() htmlText: string;

  constructor() {}
}
