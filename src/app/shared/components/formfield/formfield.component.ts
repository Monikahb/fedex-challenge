import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy, QueryList, ContentChildren, Input, HostBinding } from '@angular/core';
import { PostfixDirective, PrefixDirective } from '@shared/directives';

@Component({
  selector: 'formfield',
  templateUrl: './formfield.component.html',
  styleUrls: ['./formfield.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormfieldComponent implements OnInit {
  @Input() direction: 'horizontal'|'vertical' = 'vertical';
  @Input() classList: string;
  @Input() size:'sm'|'lg' = null;
  @HostBinding('class.formfield') hostClass = true;
  @ContentChildren(PrefixDirective) _prefixChildren: QueryList<PrefixDirective>;
  @ContentChildren(PostfixDirective) _postfixChildren: QueryList<PostfixDirective>;

  constructor() { }

  ngOnInit() { }
}
