import { ErrorMsgDirective, PostfixDirective, PrefixDirective } from './directives';
import { FormfieldComponent, AlertComponent } from './components';

export const SHARED_DECLARATIONS = [
    FormfieldComponent,
    AlertComponent,
    ErrorMsgDirective,
    PostfixDirective,
    PrefixDirective
]