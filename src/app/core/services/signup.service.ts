import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { JsonHttp } from './json-http.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class SignupService {
  http: JsonHttp;

  /**
   * 
   */
  constructor(http: JsonHttp) {
    this.http = http;
  }

  /**
   * 
   */
  signUp(data): Observable<Response> {
    const body = JSON.stringify(data);

    return this.http.post(`${environment.API_URL}`, body);
  }
}
