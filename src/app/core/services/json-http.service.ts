import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { catchError, map } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { _throw } from 'rxjs/observable/throw';
import { HttpClient, HttpHeaders, HttpRequest, HttpParams } from '@angular/common/http';

@Injectable()
export class JsonHttp {
  requestCache = {
    method: '',
    url: '',
    body: {},
    options: {}
  };

  constructor(
    private httpService: HttpClient) { }

  post(url: string, body: any, options?: {params?: HttpParams; header?: HttpHeaders}): Observable<any> {
    return this.httpService
      .post(url, body, { observe: 'response' })
      .pipe(
        map((res: any) => {
          if ( res.status === 200 || res.status === 202 ) {
            return res.status;
          }

          return res;
        }),
        catchError((err, caught) => this.exceptionHandler(err, caught))
      );
  }

  /**
   * Attempt to refresh token and make the request again or re throw error the
   * error for processing further up the stream.
   *
   * @param error
   * @param caughtObservable
   */
  private exceptionHandler(error: Response|any, caughtObservable: Observable<any>): Observable<any> {
    if (error.status === 401) {
      localStorage.removeItem('vsuite/apiToken');
      localStorage.removeItem('vsuite/refreshToken');
      return of(false);
    }

    // Re-throw the error
    return _throw(error);
  }
}
