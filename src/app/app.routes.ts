import { Routes } from '@angular/router';
import { SignupComponent } from './main/component/signup/signup.component';


export const routes: Routes = [
  {
    path: '',
    component: SignupComponent,
    pathMatch: 'full',
  }
];
